tea web framework
====================
 This is tea php web framework

## Configuration
In configs/globals_config.php, set SITE_DIR value
In configs/db_config.php, set db username, password and name
 
## Add new page
 create [your_page_name].php in root document
 go in the index method of [your_page_name].php and add this line:
 ```
 // get the html template
$template = get_class($this);
// send view
$this->controller->view($template);
 ```


