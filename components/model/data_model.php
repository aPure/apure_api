<?php
//定義這個class 在那一個範圍內
namespace components\model;
use components\basic\m\basic_page_model;

class  data_model {
	
	function __construct(){
		$this -> model = new basic_page_model();
	}

	function __destruct(){

	}               

	function index(){
		// Taking data from data source (here database)
		// $sql = "select 
		// 			(select count(*) from users where store='精明門市') as store_精明門市,
		// 			(select count(*) from users where store='信義門市') as store_信義門市,
		// 			(select count(*) from users where store='頂溪門市') as store_頂溪門市,
		// 			(select count(*) from users where store='新光站前') as store_新光站前,
		// 			(select count(*) from users where store='新光信義A11') as store_新光信義A11,
		// 			(select count(*) from users where store='台中新光三越') as store_台中新光三越,
		// 			(select count(*) from users where store='桃園遠百') as store_桃園遠百,
		// 			(select count(*) from users where store='高雄大遠百') as store_高雄大遠百,
		// 			(select count(*) from users where store='漢神巨蛋') as store_漢神巨蛋,
		// 			(select count(*) from users where store='台中中友') as store_台中中友,
		// 			(select count(*) from users where store='新光台南西門') as store_新光台南西門,
		// 			(select count(*) from users where store='中壢SOGO') as store_中壢SOGO,
		// 			(select count(*) from users where store='台中大遠百') as store_台中大遠百,
		// 			(select count(*) from users where store='板橋中山遠百') as store_板橋中山遠百,
		// 			(select count(*) from users where store='高雄新光左營') as store_高雄新光左營,
		// 			(select count(*) from users where store='新竹巨城') as store_新竹巨城,
		// 			(select count(*) from users where store='新光南西') as store_新光南西,
		// 			(select count(*) from users where store='新光信義A8') as store_新光信義A8,
		// 			(select count(*) from users where store='高雄夢時代') as store_高雄夢時代,
		// 			(select count(*) from users where store='復興SOGO') as store_復興SOGO,
		// 			(select count(*) from users where store='大江購物中心') as store_大江購物中心,
		// 			(select count(*) from users) as store_all";
		$sql = "select 
					(select count(*) from users where store='新光三越A11–5F' and event='3') as store_新光三越A11,
					(select count(*) from users where store='台中新光三越-12F' and event='3') as store_台中新光三越,
					(select count(*) from users where store='桃園遠百-8F' and event='3') as store_桃園遠百,
					(select count(*) from users where store='高雄大遠百-9F' and event='3') as store_高雄大遠百,
					(select count(*) from users where store='高雄漢神巨蛋-8F' and event='3') as store_高雄漢神巨蛋,
					(select count(*) from users where store='新光三越南西-8F' and event='3') as store_新光三越南西,
					(select count(*) from users where store='新光三越A8–B1' and event='3') as store_新光三越A8,
					(select count(*) from users where event='3') as store_all";
		$this->model->basic_select('data', 'stores_hit', $sql);
		// End taking data
		return $this -> model-> data;
	}

}