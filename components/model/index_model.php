<?php
//定義這個class 在那一個範圍內
namespace components\model;
use components\basic\m\basic_page_model;

class  index_model {

	function __construct(){
		$this -> model = new basic_page_model();
	}

	function __destruct(){

	}

	function index(){
		// Taking data from data source (here database)
		//$sql = "select * from article";
		//$this->model->basic_select('data', 'article', $sql);
		// End taking data
		//return $this -> model-> data;
		return 0;
	}
	function buabue_result(){
		$sql = "select
		(SELECT count(*) FROM apure_plus.buabue) as total,
		(select count(*) from apure_plus.buabue where picked <= 750 )  as num_1,
		(select count(*) from apure_plus.buabue where picked <= 3000 and picked >= 751)  as num_2,
		(select count(*) from apure_plus.buabue where picked <= 4750 and picked >= 3001)  as num_3,
		(select count(*) from apure_plus.buabue where picked >= 4751 )  as num_4";
		$this->model->basic_select('data', 'result', $sql);
		return $this -> model-> data['result'][0];
	}
	function addUser(){
		if($this->userExist() == 0){
			if($this->codeExist() >= 1){
				if($_REQUEST['picked'] <=750){
					$sql = "insert into apure_plus.buabue (fbid,picked) values('".$_REQUEST['fbid']."', (select id FROM apure_plus.buabue_codes
					where type='1' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 1))";
					$this->model->basic_sql_run($sql);
					$sql = "select code from buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
					$this->model->basic_select('data', 'check', $sql);
					return $this -> model-> data['check'][0]['code'];
                }
                if($_REQUEST['picked'] <=3000 && $_REQUEST['picked'] >=751){
                    $sql = "insert into apure_plus.buabue (fbid,picked) values('".$_REQUEST['fbid']."', (select id FROM apure_plus.buabue_codes
					where type='2' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 1))";
					$this->model->basic_sql_run($sql);
					$sql = "select code from buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
					$this->model->basic_select('data', 'check', $sql);
					return $this -> model-> data['check'][0]['code'];
                }
                if($_REQUEST['picked'] <=4750 && $_REQUEST['picked'] >=3001){
                    $sql = "insert into apure_plus.buabue (fbid,picked) values('".$_REQUEST['fbid']."', (select id FROM apure_plus.buabue_codes
					where type='3' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 1))";
					$this->model->basic_sql_run($sql);
					$sql = "select code from buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
					$this->model->basic_select('data', 'check', $sql);
					return $this -> model-> data['check'][0]['code'];
                }
                if($_REQUEST['picked'] >=4751){
                    $sql = "insert into apure_plus.buabue (fbid,picked) values('".$_REQUEST['fbid']."', (select id FROM apure_plus.buabue_codes
					where type='4' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 1))";
					$this->model->basic_sql_run($sql);
					$sql = "select code from buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
					$this->model->basic_select('data', 'check', $sql);
					return $this -> model-> data['check'][0]['code'];
				}
				
			}else{
				$sql = "insert into buabue (fbid,picked) values('".$_REQUEST['fbid']."', '".$_REQUEST['picked']."')";
				$this->model->basic_sql_run($sql);
				$sql = "select code from buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
				$this->model->basic_select('data', 'check', $sql);
				return $this -> model-> data['check'][0]['code'];
			}
			
		}else{
			return 0;
		}
		
	}
	function codeExist(){
		$sql = "select count(*) as num from apure_plus.buabue where picked='".urldecode($_REQUEST['picked'])."'";
		$this->model->basic_select('data', 'code', $sql);
		return intval($this -> model-> data['code'][0]['num']);
	}

	function userExist(){
		$sql = "select count(*) as num from apure_plus.buabue where fbid='".urldecode($_REQUEST['fbid'])."'";
		$this->model->basic_select('data', 'exist', $sql);
		return $this -> model-> data['exist'][0]['num'];
	}
	function checkUser(){
		$sql = "select code,type from apure_plus.buabue_codes where id =(select picked from buabue where fbid='".urldecode($_REQUEST['fbid'])."')";
		$this->model->basic_select('data', 'check', $sql);
		
		if(count($this -> model-> data['check']) == 1){
			return $this -> model-> data['check'][0];
		}else{
			$sql_numbers = array();
			$sql_numbers_sql = "select id FROM apure_plus.buabue_codes
			where type='1' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 3";
			$this->model->basic_select('data', 'numbers_1', $sql_numbers_sql);
			if(count($this -> model-> data['numbers_1'])==3){
				array_push($sql_numbers, $this -> model-> data['numbers_1']);
			}else{
				array_push($sql_numbers, $this -> model-> data['numbers_1']);
				for ($i=0; $i <3-count($this -> model-> data['numbers_1']) ; $i++) { 
					array_push($sql_numbers, $this -> model-> data['numbers_1'][0]);
				}
			}

			$sql_numbers_sql = "select id FROM apure_plus.buabue_codes
			 where type='2' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 9";
			$this->model->basic_select('data', 'numbers_2', $sql_numbers_sql);
			if(count($this -> model-> data['numbers_2'])==9){
				array_push($sql_numbers, $this -> model-> data['numbers_2']);
			}else{
				array_push($sql_numbers, $this -> model-> data['numbers_2']);
				for ($i=0; $i <9-count($this -> model-> data['numbers_2']) ; $i++) { 
					array_push($sql_numbers, $this -> model-> data['numbers_2'][0]);
				}
			}

			$sql_numbers_sql = "select id FROM apure_plus.buabue_codes
			 where type='3' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 7";
			$this->model->basic_select('data', 'numbers_3', $sql_numbers_sql);
			if(count($this -> model-> data['numbers_3'])==7){
				array_push($sql_numbers, $this -> model-> data['numbers_3']);
			}else{
				array_push($sql_numbers, $this -> model-> data['numbers_3']);
				for ($i=0; $i <7-count($this -> model-> data['numbers_3']) ; $i++) { 
					array_push($sql_numbers, $this -> model-> data['numbers_3'][0]);
				}
			}

			$sql_numbers_sql = "select id FROM apure_plus.buabue_codes
			 where type='4' and id not in (select * from (select picked from apure_plus.buabue) as non_picked) limit 1";
			$this->model->basic_select('data', 'numbers_4', $sql_numbers_sql);
			array_push($sql_numbers, $this -> model-> data['numbers_4']);
			

			return $sql_numbers;
		}
		
	}
	
	// function add_barcode_info_db($filename){
	// 	$sql = "insert into barcodes (filename) values('".$filename."')";
	// 	$this->model->basic_sql_run($sql);
	// }

	// function add_barcode(){
	// 	$target_dir = "uploads/";
	// 	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	// 	$uploadOk = 1;

	// 	if (file_exists($target_file)) {
	// 		$this -> model-> data['upload'] = -2;
	// 		return $this -> model-> data;
	// 	}

	// 	if ($uploadOk == 0) {
	// 		$this -> model-> data['upload'] = 0;
	// 		//echo "Sorry, your file was not uploaded.";
	// 	// if everything is ok, try to upload file
	// 	} else {
	// 		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	// 			$this -> model-> data['upload'] = 1;
	// 			//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	// 		} else {
	// 			$this -> model-> data['upload'] = -1;
	// 			//echo "Sorry, there was an error uploading your file.";
	// 		}
	// 	}

	// 	$this->add_barcode_info_db($target_file);

	// 	return $this -> model-> data;
	// }
	function modify(){
		$sql = "update users_feb set name='".urldecode($_REQUEST['name'])."', cel='".urldecode($_REQUEST['cel'])."' where id='".urldecode($_REQUEST['user'])."'";
		$this->model->basic_sql_run($sql);
		// End taking data
		return $this -> model-> data;
    }
    function delete(){
        $sql = "delete from users_feb where id='".urldecode($_REQUEST['user'])."'";
		//echo $sql; exit;
		$this->model->basic_sql_run($sql);
		// End taking data
		return $this -> model-> data;
    }
	function event_result(){
		// Taking data from data source (here database)
		$sql = "select id, name, cel, code, store, time from users_feb order by time desc";
		$this->model->basic_select('data', 'users', $sql);
		// End taking data
		return $this -> model-> data;
	}
	function previous_result(){
		// Taking data from data source (here database)
		$sql = "select id, name, cel, code, store, time from users where event='1' order by time desc";
		$this->model->basic_select('data', 'users', $sql);
		// End taking data
		return $this -> model-> data;
	}
	function check(){
		$sql = "select * from users_feb where code='".urldecode($_REQUEST['code'])."'";
		$this->model->basic_select('data', 'check', $sql);
		return $this -> model-> data;
	}
	function map_store_number($storename){
		switch($storename){
			// case '新光三越A11–5F':
			// 	return '16002';
			// 	break;
			// case '台中新光三越-12F':
			// 	return '16003';
			// 	break;
			// case '桃園遠百-8F':
			// 	return '16007';
			// 	break;
			// case '高雄大遠百-9F':
			// 	return '16011';
			// 	break;
			// case '高雄漢神巨蛋-8F':
			// 	return '16016';
			// 	break;
			// case '新光三越南西-8F':
			// 	return '16038';
			// 	break;
			// case '新光三越A8–B1':
			// 	return '16045';
			// 	break;
			/*
			* Event 4
			*/
			case '精明門市':
				return '15001';
				break;
			case '信義門市':
				return '15002';
				break;
			case '頂溪門市':
				return '15014';
				break;
			case '新光站前':
				return '16001';
				break;
			case '新光信義A11':
				return '16002';
				break;
			case '台中新光三越':
				return '16003';
				break;
			case '桃園遠百':
				return '16007';
				break;
			case '高雄大遠百':
				return '16011';
				break;
			case '漢神巨蛋':
				return '16016';
				break;
			case '台中中友':
				return '16017';
				break;
			case '新光台南西門':
				return '16024';
				break;
			case '中壢SOGO':
				return '16028';
				break;
			case '台中大遠百':
				return '16029';
				break;
			case '板橋中山遠百':
				return '16032';
				break;
			case '高雄新光左營':
				return '16033';
				break;
			case '新竹巨城':
				return '16037';
				break;
			case '新光南西':
				return '16038';
				break;
			case '新光信義A8':
				return '16045';
				break;
			case '高雄夢時代':
				return '16048';
				break;
			case '復興SOGO':
				return '16049';
				break;
			case '大江購物中心':
				return '16050';
				break;
		}
	}
	function event(){
		$sql = "select * from users_feb where cel='".urldecode($_REQUEST['cel'])."'";
		$this->model->basic_select('data', 'user', $sql);
		if(count($this -> model-> data['user']) >= 1){
			if($this -> model-> data['user'][0]['done'] == '1'){
				$this -> model-> data['check'] = 2;
				return $this -> model-> data;
			}
			$this -> model-> data['check'] = 0;
			$sql = "update users_feb set done='1' where cel='".urldecode($_REQUEST['cel'])."'";
			$this->model->basic_sql_run($sql);
			$sql = "update users_feb set name='".urldecode($_REQUEST['name'])."' where cel='".urldecode($_REQUEST['cel'])."'";
			$this->model->basic_sql_run($sql);
			$store = urldecode($_REQUEST['store']);
			$code = $this->map_store_number($store);
			$this -> model-> data["code"] = $code."-0".$this -> model-> data['user'][0]['id'];
		}else{
			$sql = "insert into users_feb (name, cel, done)
						values ('".urldecode($_REQUEST['name'])."', '".urldecode($_REQUEST['cel'])."', '1')";
			$this->model->basic_sql_run($sql);
			$this -> model-> data['check'] = 0;
			$sql = "select * from users_feb where cel='".urldecode($_REQUEST['cel'])."'";
			$this->model->basic_select('data', 'user', $sql);
			$store = urldecode($_REQUEST['store']);
			$code = $this->map_store_number($store);
			$this -> model-> data["code"] = $code."-0".$this -> model-> data['user'][0]['id'];
		}
		return $this -> model-> data;
	}
	//function event(){
		//$store = urldecode($_REQUEST['store']);
		//$sql = "select * from users_3 where cel='".urldecode($_REQUEST['cel'])."'";
		//$this->model->basic_select('data', 'check', $sql);
		//$sql = "select * from users_1 where cel='".urldecode($_REQUEST['cel'])."'";
		// $this->model->basic_select('data', 'check_1', $sql);
		//if(count($this -> model-> data['check']) >= 1){
			// $sql = "insert into users_3 (name, cel, store)
			// 			values ('".urldecode($_REQUEST['name'])."', '".urldecode($_REQUEST['cel'])."', '".$store."')";
			// $this->model->basic_sql_run($sql);
		//	$this -> model-> data['check'] = 1;
		//	return $this -> model-> data;
		//}
		// else if(count($this -> model-> data['check_1']) >= 1){
		// 	$sql = "insert into users_2 (name, cel, store, check_done)
		// 				values ('".urldecode($_REQUEST['name'])."', '".urldecode($_REQUEST['cel'])."', '".$store."', '3')";
		// 	$this->model->basic_sql_run($sql);
		// 	$this -> model-> data['check'] = 3;
		// 	return $this -> model-> data;
		// }
		//else{
			// $sql = "select * from users where event='3' and store='".urldecode($_REQUEST['store'])."'";
			// $this->model->basic_select('data', 'total', $sql);
			// if(count($this -> model-> data['total']) >= 300){
			// 	$sql = "insert into users_2 (name, cel, store, check_done)
			// 			values ('".urldecode($_REQUEST['name'])."', '".urldecode($_REQUEST['cel'])."', '".$store."', '2')";
			// 	$this->model->basic_sql_run($sql);
			//$this -> model-> data['check'] = 2;
			//return $this -> model-> data;
			// }else{
			//$this -> model-> data['check'] = 0;
			//$store = urldecode($_REQUEST['store']);
			//$code = $this->map_store_number($store);
			//$sql = "select id from users_3 order by id desc limit 1";
			//$this->model->basic_select('data', 'insert_id', $sql);
			//$this -> model-> data["code"] = $code."-0".++$this -> model-> data['insert_id'][0]['id'];
			//$code = $this -> model-> data["code"];
			//$sql = "insert into users_3 (name, cel, code, store)
			//		values ('".urldecode($_REQUEST['name'])."', '".urldecode($_REQUEST['cel'])."', '".$code."', '".$store."')";
			//$this->model->basic_sql_run($sql);

			//return $this -> model-> data;
			// }

		//}
	//}
}
