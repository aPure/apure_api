<?php
include_once 'configs/config.php';
use components\model\data_model;

class data extends dispatcher{

    function __construct(){ 	
        $this->data_model = new data_model();
        parent::__construct();
    }    

    function index(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        // get some data from data source
        $data = $this->data_model->index();
		//send only some data
        $this->controller->response($data);
        //send the name of this class to view its template
        //$this->controller->view(get_class($this));
    }

}

if (class_exists(data)){
    $start_classes =new data();
}
