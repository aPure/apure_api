<?php
include_once 'configs/config.php';
use components\model\index_model;

class index extends dispatcher {

    function __construct(){ 	
        $this->data_model = new index_model();
        parent::__construct();
    }    

    function index(){
        // get some data from data source
        //$data = $this->data_model->index();
		//send only some data
        //$this->controller->response($data);
        //send the name of this class to view its template
        $this->controller->view(get_class($this));
    }
    function addUser(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->addUser();
        $this->controller->responseText($data);
    }
    function checkUser(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->checkUser();
        $this->controller->response($data);
    }
    function codeExisttest(){
        $data = $this->data_model->codeExist();
        $this->controller->response($data); 
    }
    function imageUpload(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $img = $_REQUEST['img'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = 'templates/img/' . uniqid() . '.png';
        $success = file_put_contents($file, $data);
        if($success) {
            $this->controller->responseText($file);
         }else{
            $this->controller->responseText("Unable to save the file");
         } 
    }

    function buabue_result(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->buabue_result();
        $this->controller->response($data);
    }
    function event(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->event();
        $randNum = rand(1000, 9999);
        $store = $_REQUEST['store'];
        //$store = str_replace("-", "", $store);
        //$store = str_replace("F", "樓", $store);
        if($data['check'] == 0){
            //$message = "%E6%84%9F%E8%AC%9D%E5%8F%83%E8%88%87aPure%E5%99%93%E5%AF%92%E5%95%8F%E6%9A%96%E5%AF%92%E6%B5%81%E5%BF%AB%E9%96%83%E6%B4%BB%E5%8B%95%EF%BC%8C%E6%82%A8%E7%9A%84%E9%A0%98%E5%8F%96%E7%B7%A8%E8%99%9F".$data['code']."%EF%BC%8C%E8%AB%8B%E5%87%BA%E7%A4%BA%E7%B0%A1%E8%A8%8A%E5%9C%A8".$store."%E9%A0%98%E5%8F%96%E8%93%84%E7%86%B1%E8%A5%AA1%E9%9B%99%EF%BC%8C%E6%AF%8F%E4%BA%BA%E9%99%901%E6%AC%A1";
            $message = "".$randNum;
            //$message="%E6%84%9F%E8%AC%9D%E5%8F%83%E8%88%87Pure5.5%E9%85%B8%E9%B9%BC%E5%B9%B3%E8%A1%A1%E8%A4%B2%E9%AB%94%E9%A9%97%E6%B4%BB%E5%8B%95%EF%BC%8C%E6%82%A8%E7%9A%84%E9%A0%98%E5%8F%96%E7%B7%A8%E8%99%9F".$data['code']."%EF%BC%8C%E8%AB%8B%E5%9C%A810%2F1%E7%95%B6%E5%A4%A9%E5%89%8D%E5%BE%80aPure".$store."%E5%87%BA%E7%A4%BA%E9%A0%98%E5%8F%96";
            $this->sendSMS(urldecode($_REQUEST['cel']), $message);
        }
        $data['valid_phone'] = "".$randNum;
        //$this->update_realtime_app();
        $this->controller->response($data);
        //$this->controller->view(get_class($this), $data);
    }
    function check(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->check();
        $this->controller->response($data['check'][0]);
    }
    function event_result(){
        $data = $this->data_model->event_result();
        //$this->controller->response($data);
        $this->controller->view('event_result', $data);
    }
    function previous_result(){
        $data = $this->data_model->previous_result();
        //$this->controller->response($data);
        $this->controller->view('event_result', $data);
    }
    function modify(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->modify();
        $this->controller->response($data);
    }
    function delete(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $data = $this->data_model->delete();
        $this->update_realtime_app();
        $this->controller->response($data);
    }
    function update_realtime_app(){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://13.113.64.137/users/update_users_this_day',
            CURLOPT_USERAGENT => 'aPure'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
    }

    function sendSMS($phoneNumber, $message){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.twsms.com/smsSend.php?username=apure&password=apure508&mobile='.$phoneNumber.'&message='.$message,
            CURLOPT_USERAGENT => 'aPure'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
    }
}

if (class_exists(index)){
    $start_classes =new index();
}
