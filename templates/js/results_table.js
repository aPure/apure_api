var server = 'http://13.115.204.182/index.php';
$(function(){
  $('#keywords').tablesorter(); 
});

function modify(id){
  var name_val = $('.name_'+id).text();
  var cel_val = $('.cel_'+id).text();
  $('.name_'+id).html("<input class='new_name_"+id+"' value='"+name_val+"'/>");
  $('.cel_'+id).html("<input class='new_cel_"+id+"' value='"+cel_val+"'/><button onclick='confirmModify("+id+")'>Confirm</button><button onclick='cancelModify("+id+")'>Cancel</button>");
}

function deletion(id){
  if(confirm("Are you sure ?")){
    $.get(server+"?action/delete/user/"+id, function( data ) {
      alert("Deleted");
      location.reload();
    });
    
    
  }
}

function confirmModify(id){
  var name_val = $('.new_name_'+id).val();
  var cel_val = $('.new_cel_'+id).val();
  $.get( server+"?action/modify/user/"+id+"/name/"+name_val+"/cel/"+cel_val, function( data ) {
      
      location.reload();
  });
  
}

function cancelModify(id){
  var name_val = $('.new_name_'+id).val();
  var cel_val = $('.new_cel_'+id).val();
  $('.name_'+id).html(name_val);
  $('.cel_'+id).html(cel_val);
}