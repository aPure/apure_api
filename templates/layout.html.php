<!DOCTYPE>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="templates/css/results_table.css">
    </head>
    <body>
        <?php
            switch($this->template){
                case 'add_barcode':
                    include_once 'add_barcode.html.php';
                    break;
                case 'event_result':
                    include_once 'event_result.html.php';
                    break;
                default:
                    include_once 'index.html.php';
                    break;
                    
            }
        ?>
    </body>
    <script type="text/javascript" src="templates/js/jquery-latest.js"></script>
    <script type="text/javascript" src="templates/js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="templates/js/results_table.js"></script>
</html>