<?php
use components\basic\c\basic_page_controller;

class dispatcher {
    protected $action;

    function __construct(){
        $this -> controller = new basic_page_controller();
      
        $action_switch = laout_check($_REQUEST['action']);
      
        switch ($action_switch) {
            case 'check':
                $this->check();
                break;
            case 'previous_result':
                $this->previous_result();
                break;
            case 'modify':
                $this->modify();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'event_result':
                $this->event_result();
                break;   
            case 'event':
                $this->event();
                break;
            case 'imageUpload':
                $this->imageUpload();
                break;
            case 'addUser':
                $this->addUser();
                break;
            case 'checkUser':
                $this->checkUser();
                break;
            case 'codeExist':
                $this->codeExisttest();
                break;
            case 'buabue_result':
                $this->buabue_result();
                break;
            default:                       
                $this->index();
                break;
        }   
 
    }   

}

